# 私有Maven库Nexus

!>Nexus是一个强大的Maven仓库管理器，它极大地简化了自己内部仓库的维护和外部仓库的访问。利用Nexus你可以只在一个地方就能够完全控制访问 和部署在你所维护仓库中的每个Artifact。

![nexuspage](assets/nexuspage.png)

地址:[http://192.168.0.184:8081/nexus](http://192.168.0.184:8081/nexus)

## 配置使用

----------

**修改Maven `setting.xml` 文件（也可下载[XML](ftp://192.168.0.184/%E6%96%87%E6%A1%A3/maven%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6/settings.xml)配置文件直接覆盖即可），添加如下配置：**

```xml
<mirrors>
	<mirror>
        <id>nexusMirror</id>
        <mirrorOf>*</mirrorOf>
        <name>Nexus</name>
        <url>http://192.168.0.184:8081/nexus/content/groups/public/</url>
    </mirror> 
</mirrors>
```

> 原来使用阿里云nexus更换url为 `http://192.168.0.184:8081/nexus/content/groups/public/` 即可

## 命名规则

----------

**[强制] 定义 GAV 遵从以下规则：**



- GroupID 格式：com.{公司/BU }.业务线.[子业务线]，最多 4 级。

说明：{公司/BU} 例如：alibaba/taobao/tmall/aliexpress 等 BU 一级；子业务线可选。

正例：com.taobao.jstorm 或 com.alibaba.dubbo.register

- ArtifactID 格式：产品线名-模块名。语义不重复不遗漏，先到中央仓库去查证一下。

正例：dubbo-client / fastjson-api / jstorm-tool

- Version：详细规定参考下方。

**[强制] 二方库版本号命名方式：`主版本号.次版本号.修订号`**

- 主版本号：当做了不兼容的 API 修改，或者增加了能改变产品方向的新功能。

- 次版本号：当做了向下兼容的功能性新增（新增类、接口等）。

- 修订号：修复 bug，没有修改方法签名的功能加强，保持 API 兼容性。

!>说明：起始版本号必须为：1.0.0，而不是 0.0.1 正式发布的类库必须先去中央仓库进行查证，使版本号有延续性，正式版本号不允许覆盖升级。如当前版本：1.3.3，那么下一个合理的版本号：1.3.4 或 1.4.0 或 2.0.0
## 引用snapshots

!> 引用nexus快照版本的jar包 需在pom.xml 文件中加入如下配置：

```xml
<repositories>
    <repository>
        <id>nexus</id>
        <name>nexus</name>
        <url>http://192.168.0.184:8081/nexus/content/groups/public/</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>
</repositories>
```

## 上传第三方JAR


----------

1.登录Nexus后台

![nexusthree](assets/nexuslogin.png)

2.选择第三方库

![nexusthree](assets/nexusthree1.bmp)

3.填写group,artifact,version等信息

![nexusthree](assets/nexusthree2.bmp)

4.上传jar包

![nexusthree](assets/nexusthree3.bmp)

5.保存


## 发布项目 


----------

1.修改Maven中 `setting.xml` 文件，在`<servers></servers>`添加如下配置:

```xml
<server>  
    <id>releases</id>  
    <username>admin</username>  
    <password>admin123</password>  
    </server>  
    <server>  
    <id>Snapshots</id>  
    <username>admin</username>  
    <password>admin123</password>  
</server>   
```

2.在maven项目的 `pom.xml` 文件中，添加如下配置：

```xml
<distributionManagement>
		<repository>
			<id>releases</id>
			<name>Local Nexus Repository</name>
			<url>http://192.168.0.184:8081/nexus/content/repositories/releases/</url>
		</repository>
		<snapshotRepository>
			<id>Snapshots</id>
			<name>Local Nexus Repository</name>
			<url>http://192.168.0.184:8081/nexus/content/repositories/snapshots</url>
		</snapshotRepository>
</distributionManagement>
```

3.`CMD`至项目根目录下执行 `mvn clean deploy  -Dmaven.test.skip=true` 命令将项目发布到nexus上

如图：


![uploadnexus](assets/uploadnexus.png)

出现`BUILD SUCCESS`即发布成功。