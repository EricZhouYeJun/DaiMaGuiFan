# 参考文档

## 后端


- [Spring](https://waylau.gitbooks.io/spring-framework-4-reference/content/)
- [Mybatis](http://www.mybatis.org/mybatis-3/zh/index.html)
- [Mybatis-spring](http://www.mybatis.org/spring/zh/index.html)
- [Druid](https://github.com/alibaba/druid/wiki/%E9%A6%96%E9%A1%B5)
- [Log4j](http://wiki.jikexueyuan.com/project/log4j/)
- [Shiro](http://wiki.jikexueyuan.com/project/shiro/)
- [JSP](http://wiki.jikexueyuan.com/project/jsp/)
- [Junit](http://wiki.jikexueyuan.com/project/junit/)
- [Maven](http://wiki.jikexueyuan.com/project/maven/)
- [Swagger](https://github.com/swagger-api/swagger-core/wiki)
- [EasyPoi](http://easypoi.mydoc.io/)


## 前端

- [Jquery](http://www.w3school.com.cn/jquery/index.asp)
- [JavaScript](http://www.w3school.com.cn/js/index.asp)
- [html](http://www.w3school.com.cn/html/index.asp)
- [css](http://www.w3school.com.cn/css/index.asp)
- [Bootstrap](http://v3.bootcss.com/)
- [Dclould](http://dev.dcloud.net.cn/mui/ui/#mask)
- [Html5plus](http://www.html5plus.org/doc/zh_cn/accelerometer.html)



## api

- [jdk7](http://docs.oracle.com/javase/7/docs/api/)



## 常用软件

[ftp://172.168.1.158/](ftp://172.168.1.158/)