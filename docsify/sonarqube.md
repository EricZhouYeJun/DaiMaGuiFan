# 代码审查工具SonarQube

!>Sonar是一个用于代码质量管理的开源平台，用于管理源代码的质量，可以从七个维度检测代码质量通过插件形式，可以支持包括java,JavaScrip等二十几种编程语言的代码质量管理与检测

![sonarpage](assets/sonarpage.png)

地址：[http://172.168.1.158:9000/projects](http://172.168.1.158:9000/projects)

关于SonarQube的更多介绍，可以参见[https://docs.sonarqube.org/display/SONAR/Documentation/](https://docs.sonarqube.org/display/SONAR/Documentation/)

## 扫描项目

----------


**扫描工具[下载](ftp://172.168.1.158/%E5%BC%80%E5%8F%91%E5%B7%A5%E5%85%B7/sonar/sonar-scanner-3.0.3.778-windows.rar)**

修改`conf`目录下 `sonar-scanner.properties`文件

```
sonar.login=zhangyang
sonar.password=000000
```

更改为自己相应的`用户名`和`密码`

>默认用户名为`名字全拼` 
>默认密码为`000000`



### Maven项目扫描


1.在`Maven`中`setting.xml`文件（也可下载[XML](ftp://172.168.1.158/%E6%96%87%E6%A1%A3/maven%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6/settings.xml)配置文件直接覆盖即可）`<profiles></profiles>`中增加如下配置：

```xml
<profile>
            <id>sonar</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <!-- Optional URL to server. Default value is http://localhost:9000 -->
                <sonar.host.url>
                  http://172.168.1.158:9000
                </sonar.host.url>
            </properties>
        </profile>
```
!> CMD中执行mvn命令提示找不到，请将mvn配置到环境变量中

2.在项目根目录执行命令行（cmd）：

```
mvn clean install
mvn sonar:sonar
```
!> 提示java版本不正确，请将JDK_HOME配置为[JDK1.8](ftp://172.168.1.158/%E5%BC%80%E5%8F%91%E5%B7%A5%E5%85%B7/jdk/jdk-8u131-windows-x64.exe)

### 非Maven项目扫描

在项目根目录执行命令行（cmd）：

```
sonar-scanner
```