#代码规范

- 前端：html、css、Javascript

- 后端:Java、数据库

- nexus的使用

- sonar的使用


后端部分参照[阿里巴巴java开发手册](https://yq.aliyun.com/articles/69327?p=1)

前端部分改自百度 EFE/FEX CSS 规范，并参考 [lezhixing](http://fe.lezhixing.com.cn/)、[Code-guide](https://github.com/mdo/code-guide)、[HTML-CSS-guide](https://github.com/doyoe/html-css-guide) 等。

基于[docsify](https://docsify.js.org/#/zh-cn/custom-navbar)构建

![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/171902_ee7c9d8a_113154.png "2017-07-19_171818.png")


