# 前后端接口规范 #

| 名   词           | 含义 |
|:----------------------|:-----------|
| 前 端           | Web前端, APP端等一切属于用户界面的这一层,处理渲染逻辑 ,接收数据,返回数据|
| 后 端          | 即服务器端, 指一切属于用户界面之下的这一层 ,处理业务逻辑,提供数据|
| 前 后 端 接 口     | 前端与后端进行数据交互的统称, 也叫做数据接口, 属于一种远程调用, 一般指前端通过HTTP(ajax)请求获取到的数据或者执行的某项操作. 为确保前后端(工程师)的协作沟通, 一般由前端和后端一起来定义接口的规范, 规范的内容一般包含接口的地址, 接口的输入参数和输出的数据格式(结构), 最终由后端来实现这些规范, 为前端提供符合规范的接口 |


### 协作流程 ###

1. 前后端相关人员一起,对照原型,根据模块及页面大概定义出接口,一个页面中有几个接口,每个接口入参和出参都是什么。
2. 接口的频繁修改要及时反馈并修改接口文档,测试数据不满足要求也要及时提出。
3. 前端根据接口文档进行开发,发现返回的数据不对,需要及时跟后端商量,由后端修改
4. 开发完成后联调和提交测试


### 接口协商守则 ###

- 接口必须返回统一的数据结构
- 接口文档返回的字段应语义清晰并配有相应的注释
- 接口返回数据即显示,前端仅做渲染逻辑处理,尽量避免业务逻辑处理的出现
- 渲染逻辑禁止跨三个以上接口调用
- 请求响应传输数据格式：JSON,JSON数据尽量简单轻量,避免多级JSON的出现
- 错误处理,发生错误能够有友好的提示,也能在调试阶段快速准备定位错误来源和原因
- 接口查询不到数据时,即空数据的情况返回给前端怎样的数据
 * 建议返回非 `null` 的对应数据类型初始值, 例如对象类型的返回空对象(`{}`), 数组类型的返回空数组(`[]`), 其他原始数据类型(`string`/`number`/`boolean`...)也使用对应的默认值
  * 这样可以减少前端很多琐碎的非空判断, 直接使用接口中的数据
- 返回数据中的图片URL是完整的地址
- 返回数据中的日期格式,推荐返回为格式化好的文字,例如：2017年1月1日
- 对于大数字（例如long类型）,为了避免精度丢失,返回给前端时应设置为字符串类型

### 接口参数 ###

- 向接口传递参数时, 如果是少量参数可以作为
  **path(@PathVariable)、query(@RequestParam)**追加到接口的 URL 中, 或者作为 Content-Type: application/x-www-form-urlencoded 放在请求体(body)中(即**form(@RequestParam)**表单提交的方式)
- 对于复杂的接口参数(例如嵌套了多层的数据结构), 推荐在 HTTP 请求体(body)中包含一个 **JSON 字符串(@RequestBody)**作为接口的参数, 并设置 Content-Type: application/json; charset=utf-8
- GET请求使用**URL**传递参数，POST请求使用**请求体**传递参数



### 接口类型 ###

|  操作行为   |  Method  |  注解 | Path |
|  ----  | ---- | ----  |----  |
| 查找  | GET | `@GetMapping` | users/{id} |
| 增加  | POST| `@PostMapping` | addUsers |
| 修改  | POST| `@PostMapping` | updateUsers |
| 删除  | POST| `@PostMapping`| deleteUsers |



----------

## 统一返回数据格式 ##

### Maven ###

```xml
<dependency>
  <groupId>com.luculent.util</groupId>
  <artifactId>api-extension</artifactId>
  <version>1.0.0</version>
</dependency>
```

### 响应格式 ###

返回的响应体类型推荐为 `Content-Type: application/json; charset=utf-8`, 返回的数据包含在 HTTP 响应体中, 是一个泛型pojo. 

### 基本格式 ###

返回类型为 `com.luculent.util.extension.api.Result`
```
{
  "code": 200,
  "msg": "操作成功",
  "data": null
}
```


### 分页格式 ###

返回类型为 `com.luculent.util.extension.api.PageResult`

```
{
  "code": 200,
  "msg": "操作成功",
  "hasNext": false,
  "pageNum": 1,
  "listData": []
}
```
### 字段说明 ###

|  字段名   |  字段类型  |  默认值 |  字段说明 |
|  ----  | ---- | ----  | ----  |
| code  | long| 200   | **状态码**<br> 必须是>=0的整数.<ul><li>200表示处理成功</li><li>非200表示发生错误时的错误码,此时可以省略 `data\listData` 字段, 并视情况输出 `msg` 字段作为补充信息</li></ul>|
| msg  | String| 操作成功  | **状态信息**<ul><li>接口调用失败时, 返回清晰的异常提示</li><li>业务处理失败时, 给予用户的友好的提示信息, 即所有给用户的提示信息都统一由后端来处理</li></ul> |
| hasNext  | boolean| true  | **是否存在下一页**<br>避免前端过度请求 |
| pageNum  | long| 1  | **当前页数**|
| data  | Object | null  | **业务数据**<br>泛型类型,禁止使用Map|
| listData  | Object | [ ]  | **分页数据**<br>泛型List类型|

### 状态码说明 ###

服务器向用户返回的状态码和提示信息（可参考`org.springframework.http.HttpStatus`）,常见的有以下一些 ：

```
200 OK：服务器成功返回用户请求的数据,该操作是幂等的（Idempotent）
202 Accepted：表示一个请求已经进入后台排队（异步任务）
400 INVALID REQUEST：用户发出的请求有错误,服务器没有进行新建或修改数据的操作,该操作是幂等的
401 Unauthorized：表示用户没有权限（令牌、用户名、密码错误）
403 Forbidden 表示用户得到授权（与401错误相对）,但是访问是被禁止的
404 NOT FOUND：用户发出的请求针对的是不存在的记录,服务器没有进行操作,该操作是幂等的
406 Not Acceptable：用户请求的格式不可得（比如用户请求JSON格式,但是只有XML格式）
410 Gone：用户请求的资源被永久删除,且不会再得到的
422 Unprocesable entity： 当创建一个对象时,发生一个验证错误
500 INTERNAL SERVER ERROR：服务器发生错误,用户将无法判断发出的请求是否成功
```

若要返回细粒度状态码，可继承`com.luculent.util.extension.api.IErrorCode`类创建返回码枚举类

例：

```
public enum ErrorCodeEnum implements IErrorCode {

	duplicateKeyExist(40000, "记录已存在"),
	genReadConfigError(40100, "代码生成器获取配置文件失败"), 
	genWriteConfigError(40101,"代码生成器修改配置文件失败"),
	genRenderTemplateError(40102,"代码生成器渲染模板失败"),
	FileUploadGetBytesError(40200, "文件上传错误"), 
	FileUploadError(40201, "文件上传错误");

	private final long code;
	private final String msg;

	ErrorCodeEnum(final long code, final String msg) {
		this.code = code;
		this.msg = msg;
	}

	@Override
	public long getCode() {
		return code;
	}

	@Override
	public String getMsg() {
		return msg;
	}

}
```


## 参与贡献 ##

- 张洋
- 井文广
- 宗超
- 芦志强 

*欢迎参与补充和修订*